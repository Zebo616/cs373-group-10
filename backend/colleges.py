from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func

# Main API call for colleges that filters, paginates, and sorts.
# Format: "/college?" followed by a list of args and their values, separated by an "&".
#   For example, "/college?studentMin=1000&studentMax=10000" will return all colleges with a student population between 1000 and 10000, inclusive.
#   Args can appear in any order.
#   The page and pageSize args are used for pagination, with pageSize * page equaling the offset and pageSize equaling the limit.
#   The sortOn arg is used to toggle sorting. If the sortOn arg is non-negative, then the list will be
#       sorted in ascending order if sortBy is "asc", and descending order otherwise.
# Return: A subset of the list of colleges that meet the specified criteria, determined by the pagination args passed in.
#   If no pagination args are used, the function returns all colleges that meet the criteria.

def getColleges(db, college):
    cols = ("name", "city", "state", "latitude", "longitude", "num_students", "median_sat", "average_aid")
    query = db.session.query(college)
    latMin = request.args.get("latMin", None)
    latMax = request.args.get("latMax", None)
    longMin = request.args.get("longMin", None)
    longMax = request.args.get("longMax", None)
    studentMin = request.args.get("studentMin", None)
    studentMax = request.args.get("studentMax", None)
    satMin = request.args.get("satMin", None)
    satMax = request.args.get("satMax", None)
    financialMin = request.args.get("financialMin", None)
    financialMax = request.args.get("financialMax", None)
    page = int(request.args.get("page", 1))
    pageSize = int(request.args.get("pageSize", 20))
    sortOn = int(request.args.get("sortOn", -1))
    sortBy = request.args.get("sortBy", None)
    if latMin:
        query = query.filter(
            college.c.latitude >= float(latMin)
        )
    if latMax:
        query = query.filter(
            college.c.latitude <= float(latMax)
        )
    if longMin:
        query = query.filter(
            college.c.longitude >= float(longMin)
        )
    if longMax:
        query = query.filter(
            college.c.longitude <= float(longMax)
        )
    if studentMin:
        query = query.filter(
            college.c.num_students >= int(studentMin)
        )
    if studentMax:
        query = query.filter(
            college.c.num_students <= int(studentMax)
        )
    if satMin:
        query = query.filter(
            college.c.median_sat >= int(satMin)
        )
    if satMax:
        query = query.filter(
            college.c.median_sat <= int(satMax)
        )
    if financialMin:
        query = query.filter(
            college.c.average_aid >= int(financialMin)
        )
    if financialMax:
        query = query.filter(
            college.c.average_aid <= int(financialMax)
        )
    if sortOn >= 0 and sortBy:
        if sortBy == "asc":
            query = query.order_by(college.c[cols[sortOn]].asc())
        else:
            query = query.order_by(college.c[cols[sortOn]].desc())
    results = (
        query.offset(pageSize * page).limit(pageSize).all()
    )
    return results


# Supplementary API call used to find the number of colleges that meet a specified criteria.
# Format: "/college/count?...", where "..." matches the argument formatting of getColleges().
# Return: The number of colleges that meet the specified criteria.

def getCollegeCount(db, college):
    query = db.session.query(func.count(college.c.id))
    latMin = request.args.get("latMin", None)
    latMax = request.args.get("latMax", None)
    longMin = request.args.get("longMin", None)
    longMax = request.args.get("longMax", None)
    studentMin = request.args.get("studentMin", None)
    studentMax = request.args.get("studentMax", None)
    satMin = request.args.get("satMin", None)
    satMax = request.args.get("satMax", None)
    financialMin = request.args.get("financialMin", None)
    financialMax = request.args.get("financialMax", None)
    if latMin:
        query = query.filter(
            college.c.latitude >= float(latMin)
        )
    if latMax:
        query = query.filter(
            college.c.latitude <= float(latMax)
        )
    if longMin:
        query = query.filter(
            college.c.longitude >= float(longMin)
        )
    if longMax:
        query = query.filter(
            college.c.longitude <= float(longMax)
        )
    if studentMin:
        query = query.filter(
            college.c.num_students >= int(studentMin)
        )
    if studentMax:
        query = query.filter(
            college.c.num_students <= int(studentMax)
        )
    if satMin:
        query = query.filter(
            college.c.median_sat >= int(satMin)
        )
    if satMax:
        query = query.filter(
            college.c.median_sat <= int(satMax)
        )
    if financialMin:
        query = query.filter(
            college.c.average_aid >= int(financialMin)
        )
    if financialMax:
        query = query.filter(
            college.c.average_aid <= int(financialMax)
        )
    results = query.first()
    return results


# Supplemental API call that returns the college with the given ID.
# Format: Shown in @app.route() below.
# Return: The college with the given ID.

def getCollege(id_num, db, college):
    results = (
        db.session.query(college).filter_by(id=id_num).all()
    )
    return results


# Supplemental API call that returns the colleges within the given city and state.
# Format: Shown in @app.route() below.
# Return: The colleges within the given city and state.

def getCollegesByLocation(city_name, state_name, db, college):
    results = (
        db.session.query(college)
        .filter_by(city=city_name)
        .filter_by(state=state_name)
        .all()
    )
    return results
