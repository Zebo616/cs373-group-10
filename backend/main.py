from flask import Flask, request, jsonify, render_template, request, send_from_directory
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func
from flask_cors import CORS
import colleges, cities, restaurants
import os

# This file combines all three API model calls for ease of access from the frontend.
# For API call descriptions, please visit their respective files.

# initialize the app

app = Flask(__name__)
CORS(app)
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "mysql+pymysql://group10:welovedowning@collegesearch-db.clloxbglqxtz.us-east-2.rds.amazonaws.com/collegesearch"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["pool_size"] = 20
app.config["max_overflow"] = 10


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def index(path):
  return render_template("index.html")

# initialize the database
db = SQLAlchemy(app)

restaurant = db.Table(
    "restaurants",
    db.metadata,
    autoload=True,
    autoload_with=db.engine,
)

college = db.Table(
    "college",
    db.metadata,
    autoload=True,
    autoload_with=db.engine,
)

city = db.Table(
    "city",
    db.metadata,
    autoload=True,
    autoload_with=db.engine,
)

@app.route("/college")
def getColleges():
    return formatResponse(
        colleges.getColleges(db, college)
    )


@app.route("/college/count")
def getCollegeCount():
    return formatResponse(
        colleges.getCollegeCount(db, college)
    )


@app.route("/college/<int:id_num>")
def getCollege(id_num):
    return formatResponse(
        colleges.getCollege(id_num, db, college)
    )


@app.route(
    "/college/<string:state_name>/<string:city_name>"
)
def getCollegesByLocation(city_name, state_name):
    return formatResponse(
        colleges.getCollegesByLocation(
            city_name, state_name, db, college
        )
    )


@app.route("/city")
def getCities():
    return formatResponse(
        cities.getCities(db, city)
    )


@app.route("/city/count")
def getCityCount():
    return formatResponse(
        cities.getCityCount(db, city)
    )


@app.route("/city/<int:id_num>")
def getCity(id_num):
    return formatResponse(
        cities.getCity(id_num, db, city)
    )


@app.route(
    "/city/<string:state_name>/<string:st_code>/<string:city_name>"
)
def getCityByName(city_name, state_name, st_code):
    return formatResponse(
        cities.getCityByName(
            city_name, state_name, st_code, db, city
        )
    )


@app.route("/restaurant")
def getRestaurants():
    return formatResponse(
        restaurants.getRestaurants(db, restaurant)
    )


@app.route("/restaurant/count")
def getRestaurantCount():
    return formatResponse(
        restaurants.getRestaurantCount(db, restaurant)
    )


@app.route("/restaurant/<int:id_num>")
def getRestaurant(id_num):
    return formatResponse(
        restaurants.getRestaurant(id_num, db, restaurant)
    )


@app.route(
    "/restaurant/<string:st_code>/<string:city_name>"
)
def getRestaurantsByLocation(city_name, st_code):
    return formatResponse(
        restaurants.getRestaurantsByLocation(
            city_name, st_code, db, restaurant
        )
    )

def formatResponse(res):
    response = jsonify(res)
    response.headers.set("Access-Control-Allow-Origin", "*")
    response.headers.set(
        "Access-Control-Allow-Methods", "GET"
    )
    return response

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000, debug=True, ssl_context=('cert.pem', 'key.pem'))
