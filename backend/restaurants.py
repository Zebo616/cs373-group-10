from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func

# Main API call for restaurants that filters and paginates.
# Format: "/restaurant?" followed by a list of args and their values, separated by an "&".
#   For example, "/restaurant?priceMin=1&priceMax=3" will return all restaurants with a price index between 1 and 3, inclusive.
#   Args can appear in any order.
#   The page and pageSize args are used for pagination, with pageSize * page equaling the offset and pageSize equaling the limit.
#   The sortOn arg is used to toggle sorting. If the sortOn arg is non-negative, then the list will be
#       sorted in ascending order if sortBy is "asc", and descending order otherwise.
# Return: A subset of the list of restaurants that meet the specified criteria, determined by the pagination args passed in.
#   If no pagination args are used, the function returns all restaurants that meet the criteria.

def getRestaurants(db, restaurant):
    cols = ("name", "address", "city", "state", "lat", "long", "price")
    query = db.session.query(restaurant)
    latMin = request.args.get("latMin", None)
    latMax = request.args.get("latMax", None)
    longMin = request.args.get("longMin", None)
    longMax = request.args.get("longMax", None)
    priceMin = request.args.get("priceMin", None)
    priceMax = request.args.get("priceMax", None)
    page = int(request.args.get("page", 1))
    pageSize = int(request.args.get("pageSize", 20))
    sortOn = int(request.args.get("sortOn", -1))
    sortBy = request.args.get("sortBy", None)
    if latMin:
        query = query.filter(
            restaurant.c.lat >= float(latMin)
        )
    if latMax:
        query = query.filter(
            restaurant.c.lat <= float(latMax)
        )
    if longMin:
        query = query.filter(
            restaurant.c.long >= float(longMin)
        )
    if longMax:
        query = query.filter(
            restaurant.c.long <= float(longMax)
        )
    if priceMin:
        query = query.filter(
            restaurant.c.price >= int(priceMin)
        )
    if priceMax:
        query = query.filter(
            restaurant.c.price <= int(priceMax)
        )
    if sortOn >= 0 and sortBy:
        if sortBy == "asc":
            query = query.order_by(restaurant.c[cols[sortOn]].asc())
        else:
            query = query.order_by(restaurant.c[cols[sortOn]].desc())
    results = (
        query.offset(pageSize * page).limit(pageSize).all()
    )
    return results


# Supplementary API call used to find the number of restaurants that meet a specified criteria.
# Format: "/restaurant/count?...", where "..." matches the argument formatting of getRestaurants().
# Return: The number of restaurants that meet the specified criteria.
def getRestaurantCount(db, restaurant):
    query = db.session.query(func.count(restaurant.c.id))
    latMin = request.args.get("latMin", None)
    latMax = request.args.get("latMax", None)
    longMin = request.args.get("longMin", None)
    longMax = request.args.get("longMax", None)
    priceMin = request.args.get("priceMin", None)
    priceMax = request.args.get("priceMax", None)
    if latMin:
        query = query.filter(
            restaurant.c.lat >= float(latMin)
        )
    if latMax:
        query = query.filter(
            restaurant.c.lat <= float(latMax)
        )
    if longMin:
        query = query.filter(
            restaurant.c.long >= float(longMin)
        )
    if longMax:
        query = query.filter(
            restaurant.c.long <= float(longMax)
        )
    if priceMin:
        query = query.filter(
            restaurant.c.price >= int(priceMin)
        )
    if priceMax:
        query = query.filter(
            restaurant.c.price <= int(priceMax)
        )
    results = query.first()
    return results


# Supplemental API call that returns the restaurant with the given ID.
# Format: Shown in @app.route() below.
# Return: The restaurant with the given ID.
def getRestaurant(id_num, db, restaurant):
    results = (
        db.session.query(restaurant)
        .filter_by(id=id_num)
        .all()
    )
    return results


# Supplemental API call that returns the restaurants within the given city and state.
# Format: Shown in @app.route() below.
# Return: The restaurants within the given city and state.
def getRestaurantsByLocation(city_name, st_code, db, restaurant):
    results = (
        db.session.query(restaurant)
        .filter_by(city=city_name)
        .filter_by(state=st_code)
        .all()
    )
    return results
