from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func

# Main API call for cities that filters and paginates.
# Format: "/city?" followed by a list of args and their values, separated by an "&".
#   For example, "/city?popMin=10000&popMax=100000" will return all cities with a population between 10000 and 100000, inclusive.
#   Args can appear in any order.
#   The page and pageSize args are used for pagination, with pageSize * page equaling the offset and pageSize equaling the limit.
#   The sortOn arg is used to toggle sorting. If the sortOn arg is non-negative, then the list will be
#       sorted in ascending order if sortBy is "asc", and descending order otherwise.
# Return: A subset of the list of cities that meet the specified criteria, determined by the pagination args passed in.
#   If no pagination args are used, the function returns all cities that meet the criteria.

def getCities(db, city):
    cols = ("city", "state", "state_code", "population", "num_veterans", "foreign_born", "average_household", "median_age")
    query = db.session.query(city)
    popMin = request.args.get("popMin", None)
    popMax = request.args.get("popMax", None)
    vetMin = request.args.get("vetMin", None)
    vetMax = request.args.get("vetMax", None)
    foreignMin = request.args.get("foreignMin", None)
    foreignMax = request.args.get("foreignMax", None)
    houseMin = request.args.get("houseMin", None)
    houseMax = request.args.get("houseMax", None)
    ageMin = request.args.get("ageMin", None)
    ageMax = request.args.get("ageMax", None)
    page = int(request.args.get("page", 1))
    pageSize = int(request.args.get("pageSize", 20))
    sortOn = int(request.args.get("sortOn", -1))
    sortBy = request.args.get("sortBy", None)
    if popMin:
        query = query.filter(
            city.c.population >= float(popMin)
        )
    if popMax:
        query = query.filter(
            city.c.population <= float(popMax)
        )
    if vetMin:
        query = query.filter(
            city.c.num_veterans >= float(vetMin)
        )
    if vetMax:
        query = query.filter(
            city.c.num_veterans <= float(vetMax)
        )
    if foreignMin:
        query = query.filter(
            city.c.foreign_born >= int(foreignMin)
        )
    if foreignMax:
        query = query.filter(
            city.c.foreign_born <= int(foreignMax)
        )
    if houseMin:
        query = query.filter(
            city.c.average_household >= int(houseMin)
        )
    if houseMax:
        query = query.filter(
            city.c.average_household <= int(houseMax)
        )
    if ageMin:
        query = query.filter(
            city.c.median_age >= int(ageMin)
        )
    if ageMax:
        query = query.filter(
            city.c.median_age <= int(ageMax)
        )
    if sortOn >= 0 and sortBy:
        if sortBy == "asc":
            query = query.order_by(city.c[cols[sortOn]].asc())
        else:
            query = query.order_by(city.c[cols[sortOn]].desc())
    results = (
        query.offset(pageSize * page).limit(pageSize).all()
    )
    return results


# Supplementary API call used to find the number of cities that meet a specified criteria.
# Format: "/city/count?...", where "..." matches the argument formatting of getCities().
# Return: The number of cities that meet the specified criteria.

def getCityCount(db, city):
    query = db.session.query(func.count(city.c.id))
    popMin = request.args.get("popMin", None)
    popMax = request.args.get("popMax", None)
    vetMin = request.args.get("vetMin", None)
    vetMax = request.args.get("vetMax", None)
    foreignMin = request.args.get("foreignMin", None)
    foreignMax = request.args.get("foreignMax", None)
    houseMin = request.args.get("houseMin", None)
    houseMax = request.args.get("houseMax", None)
    ageMin = request.args.get("ageMin", None)
    ageMax = request.args.get("ageMax", None)
    if popMin:
        query = query.filter(
            city.c.population >= float(popMin)
        )
    if popMax:
        query = query.filter(
            city.c.population <= float(popMax)
        )
    if vetMin:
        query = query.filter(
            city.c.num_veterans >= float(vetMin)
        )
    if vetMax:
        query = query.filter(
            city.c.num_veterans <= float(vetMax)
        )
    if foreignMin:
        query = query.filter(
            city.c.foreign_born >= int(foreignMin)
        )
    if foreignMax:
        query = query.filter(
            city.c.foreign_born <= int(foreignMax)
        )
    if houseMin:
        query = query.filter(
            city.c.average_household >= int(houseMin)
        )
    if houseMax:
        query = query.filter(
            city.c.average_household <= int(houseMax)
        )
    if ageMin:
        query = query.filter(
            city.c.median_age >= int(ageMin)
        )
    if ageMax:
        query = query.filter(
            city.c.median_age <= int(ageMax)
        )
    results = query.first()
    return results


# Supplemental API call that returns the city with the given ID.
# Format: Shown in @app.route() below.
# Return: The city with the given ID.

def getCity(id_num, db, city):
    results = (
        db.session.query(city).filter_by(id=id_num).all()
    )
    return results


# Supplemental API call that returns the city with the given name in a certain state.
# Format: Shown in @app.route() below.
#   The state code (st_code) argument is optional. If no st_code is given, the state name will be searched instead.
# Return: the city with the given name in the given state.

def getCityByName(city_name, state_name, st_code, db, city):
    if st_code == "empty":
        results = (
            db.session.query(city)
            .filter_by(city=city_name)
            .filter_by(state=state_name)
            .first()
        )
    else:
        results = (
            db.session.query(city)
            .filter_by(city=city_name)
            .filter_by(state_code=st_code)
            .first()
        )
    return results
