from main import app
import unittest
from flask import Flask


class FlaskTest(unittest.TestCase):
    def test_college(self):
        tester = app.test_client(self)
        response = tester.get("/college")
        statuscode = response.status_code
        self.assertEqual(statuscode, 200)

    def test_college_type(self):
        tester = app.test_client(self)
        response = tester.get("/college")
        self.assertEqual(
            response.content_type, "application/json"
        )

    def test_college_data(self):
        tester = app.test_client(self)
        response = tester.get("/college/100654")
        self.assertEqual(
            response.content_type, "application/json"
        )

    def test_city(self):
        tester = app.test_client(self)
        response = tester.get("/city")
        statuscode = response.status_code
        self.assertEqual(statuscode, 200)

    def test_city_type(self):
        tester = app.test_client(self)
        response = tester.get("/city")
        self.assertEqual(
            response.content_type, "application/json"
        )

    def test_city_data(self):
        tester = app.test_client(self)
        response = tester.get("/city/8191")
        self.assertEqual(
            response.content_type, "application/json"
        )

    def test_restaurant(self):
        tester = app.test_client(self)
        response = tester.get("/restaurant")
        statuscode = response.status_code
        self.assertEqual(statuscode, 200)

    def test_restaurant_type(self):
        tester = app.test_client(self)
        response = tester.get("/restaurant")
        self.assertEqual(
            response.content_type, "application/json"
        )

    def test_restaurant_data(self):
        tester = app.test_client(self)
        response = tester.get("/restaurant/255")
        self.assertEqual(
            response.content_type, "application/json"
        )


if __name__ == "__main__":
    unittest.main()
