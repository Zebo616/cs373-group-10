from main import app
import unittest
from flask import Flask, request, jsonify
import json


class FilterTest(unittest.TestCase):

    # tests if the college filter correctly paginates results.
    def test_college_pagination(self):
        tester = app.test_client(self)
        # get result from filterColleges
        response = tester.get(
            "/college"
        )
        data = json.loads(response.get_data(as_text=True))
        print("test_college_pagination\n")
        num = 0
        for entry in data:
            # the following line does not work on the pipeline, but can be used
            # on a local machine to help debug:
            # print(entry['name'])
            num += 1
        assert num == 20
    
    # tests if the city filter correctly paginates results.
    def test_city_pagination(self):
        tester = app.test_client(self)
        # get result from filterColleges
        response = tester.get(
            "/college"
        )
        data = json.loads(response.get_data(as_text=True))
        print("test_city_pagination\n")
        num = 0
        for entry in data:
            # the following line does not work on the pipeline, but can be used
            # on a local machine to help debug:
            # print(entry['name'])
            num += 1
        assert num == 20
    
    # tests if the restaurant filter correctly paginates results.
    def test_restaurant_pagination(self):
        tester = app.test_client(self)
        # get result from filterColleges
        response = tester.get(
            "/college"
        )
        data = json.loads(response.get_data(as_text=True))
        print("test_restaurant_pagination\n")
        num = 0
        for entry in data:
            # the following line does not work on the pipeline, but can be used
            # on a local machine to help debug:
            # print(entry['name'])
            num += 1
        assert num == 20

    # tests if the college filter method works for ranges of latitude and longitude.
    def test_college_filter_by_location(self):
        tester = app.test_client(self)
        # get result from getColleges
        response = tester.get(
            "/college?latMin=34.8&latMax=35"
        )
        data = json.loads(response.get_data(as_text=True))
        print("test_college_filter_by_location\n")
        num = 0
        for entry in data:
            # the following line does not work on the pipeline, but can be used
            # on a local machine to help debug:
            # print(entry['name'])
            num += 1
        assert num == 7

    # tests if college filtering by the number of students,
    # the median SAT score, and average financial aid works as intended.
    def test_college_filter_by_status(self):
        tester = app.test_client(self)
        # get result from filterColleges
        response = tester.get(
            "/college?studentMin=100&studentMax=1000&satMin=1000&satMax=1200&financialMin=0&financialMax=10000"
        )
        data = json.loads(response.get_data(as_text=True))
        print("test_college_filter_by_status\n")
        num = 0
        for entry in data:
            # the following line does not work on the pipeline, but can be used
            # on a local machine to help debug:
            # print(entry['name'])
            num += 1
        assert num == 17

    # tests if city filtering by various population values works as intended.
    def test_city_filter_by_population(self):
        tester = app.test_client(self)
        # get result from filterCities
        response = tester.get(
            "/city?popMin=90000&popMax=100000&veteranMin=100&veteranMax=1000&foreignMin=10000&foreignMax=1000000"
        )
        data = json.loads(response.get_data(as_text=True))
        print("test_city_filter_by_population\n")
        num = 0
        for entry in data:
            # the following line does not work on the pipeline, but can be used
            # on a local machine to help debug:
            # print(entry['city'])
            num += 1
        assert num == 11

    # tests if city filtering by average income and median age works as intended.
    def test_city_filter_by_typicals(self):
        tester = app.test_client(self)
        # get result from filterCities
        response = tester.get(
            "/city?householdMin=.1&householdMax=2&ageMin=42&ageMax=45"
        )
        data = json.loads(response.get_data(as_text=True))
        print("test_city_filter_by_typicals\n")
        num = 0
        for entry in data:
            # the following line does not work on the pipeline, but can be used
            # on a local machine to help debug:
            # print(entry['city'])
            num += 1
        assert num == 10

    # tests if the restaurant filter works as intended.
    def test_restaurant_filter(self):
        tester = app.test_client(self)
        # get result from filterRestaurants
        response = tester.get(
            "/restaurant?latMin=24&latMax=25&longMin=-82&longMax=-81&priceMin=2&priceMax=4"
        )
        data = json.loads(response.get_data(as_text=True))
        print("test_restaurant_filter\n")
        num = 0
        for entry in data:
            # the following line does not work on the pipeline, but can be used
            # on a local machine to help debug:
            # print(entry['name'])
            num += 1
        assert num == 1


if __name__ == "__main__":
    unittest.main()
