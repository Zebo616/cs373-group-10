Name, EID, and GitLab ID, of all members
- Steven Callahan: svc434 @Zebo616
- Anshuman Kumar: ak37793 @anshk00
- Christopher Chen: ccc4456 @chrischen88 
- Katherine Hsu: kah4763 @katherinehsu
- Keegan Franklin: kdf937 @KeeganFranklin 

Git SHA: d7d5392217b4f99f7891126ca23cbd1d6201f233

Project leader: Anshuman Kumar

Link to GitLab pipelines: https://gitlab.com/Zebo616/cs373-group-10/-/pipelines

Link to website: https://collegesearch.me

Estimated completion time for each member (hours: int)
- Steven Callahan: 24 hours 
- Anshuman Kumar: 36 hours
- Christopher Chen: 35 hours
- Katherine Hsu: 18 hours
- Keegan Franklin: 20 hours

Actual completion time for each member (hours: int)
- Steven Callahan: 22 hours
- Anshuman Kumar: 24 hours
- Christopher Chen: 25 hours
- Katherine Hsu: 16 hours
- Keegan Franklin: 20 hours

Comments:
- Steven Callahan: I think we all learned a lot about real-world software development throughout this project!
- Anshuman Kumar: None
- Christopher Chen: None
- Katherine Hsu: None
- Keegan Franklin: None
