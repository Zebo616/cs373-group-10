import { useEffect, useState } from 'react'
import { Button, Form } from 'react-bootstrap'
import Select from 'react-select'

function buildFilters(filters, queryParams) {
    let temp = []
    for (let i = 0; i < filters.length; ++i) {
        temp.push(
            <div className="col d-flex flex-column">
                <div className="text-center mb-2">
                    <b>{filters[i].name}</b>
                </div>
                <input
                    className="mb-2"
                    type="text"
                    placeholder="From"
                    onChange={(e) =>
                        (queryParams[filters[i].min] = e.target.value)
                    }
                />
                <input
                    type="text"
                    placeholder="To"
                    onChange={(e) =>
                        (queryParams[filters[i].max] = e.target.value)
                    }
                />
            </div>
        )
    }
    return temp
}

export default function Filters(props) {
    return (
        <div>
            <div className="row">
                {buildFilters(props.filters, props.queryParams)}
            </div>
            <br />
            <div className="row justify-content-center">
                <div className="mr-4" style={{ width: '250px' }}>
                    <Select
                        options={props.columns}
                        defaultValue={props.columns[0]}
                        onChange={(e) => {
                            props.queryParams["sortOn"] = e ? e.value : null
                        }}
                    />
                    <div className="row pl-4">
                        <Form onChange={(e) => {props.queryParams["sortBy"] = e.target.value}}>
                            <Form.Check inline type="radio" label="ascending" name="sort" value="asc" defaultChecked/>
                            <Form.Check inline type="radio" label="descending" name="sort" value="desc"/>
                        </Form>
                    </div>
                </div>
                <Button onClick={props.signal}>  Update  </Button>
            </div>
        </div>
    )
}
