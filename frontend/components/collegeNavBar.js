import { Navbar, Nav, Form, Button, FormControl } from 'react-bootstrap'
import { Router, useRouter } from 'next/router'
import { useState } from 'react'

export default function CollegeNavBar() {
    const [query, setQuery] = useState()
    const router = useRouter()

    return (
        <div>
            <Navbar bg="dark" variant="dark">
                <Navbar.Brand href="/">collegesearch</Navbar.Brand>
                <Nav className="mr-auto">
                    <Nav.Link href="/about">About</Nav.Link>
                    <Nav.Link href="/colleges">Colleges</Nav.Link>
                    <Nav.Link href="/cities">Cities</Nav.Link>
                    <Nav.Link href="/restaurants">Restaurants</Nav.Link>
                    <Nav.Link href="/charts">Charts</Nav.Link>
                </Nav>
                <div className="mr-1" style={{ width: '250px' }}>
                    <FormControl
                        type="text"
                        placeholder="Search"
                        className="mr-sm-2"
                        width={100}
                        onChange={(e) => setQuery(e.target.value)}
                        onKeyPress={(e) => {
                            if (e.key === 'Enter') {
                                router.push({
                                    pathname: '/search',
                                    query: {
                                        query: query,
                                    },
                                })
                            }
                        }}
                    />
                </div>
                <Button
                    variant="outline-light"
                    onClick={function () {
                        router.push({
                            pathname: '/search',
                            query: {
                                query: query,
                            },
                        })
                    }}
                >
                    Search
                </Button>
            </Navbar>
        </div>
    )
}
