import { useState, useEffect } from 'react'
import useSWR from 'swr'
import Image from 'next/image'
import { Spinner } from 'react-bootstrap'

const fetcher = (...args) => fetch(...args).then((res) => res.json())

//Requires parameters:
// placeInput: name of what we're looking for
// maxHeight, maxWidth: height and width of image. Need both.

function PlacePhoto(props) {
    let placeInput = encodeURIComponent(props.placeInput)
    const { data, error } = useSWR(
        `/api/places?placeInput=${placeInput}&maxHeight=${props.maxHeight}&maxWidth=${props.maxWidth}`,
        fetcher
    )

    if (!data) {
        return <Spinner>Loading images</Spinner>
    }

    const { url, html_attributions } = data
    if (html_attributions.length == 0) {
        html_attributions = ['<div></div>']
    } else {
        html_attributions[0] = 'Credit to: ' + html_attributions[0]
    }

    return (
        <React.Fragment>
            <Image
                src={url}
                height={props.maxHeight}
                width={props.maxWidth}
            ></Image>
            <div
                className="Container"
                dangerouslySetInnerHTML={{ __html: html_attributions[0] }}
            />
        </React.Fragment>
    )
}

export default PlacePhoto
