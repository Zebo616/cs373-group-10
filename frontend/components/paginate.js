import useSWR from 'swr'
import Pagination from '@material-ui/lab/Pagination'

export default function Paginate(props) {
    const fetcher = (...args) =>
        fetch(...args)
            .then((res) => res.json())
            .then((data) => {
                return Math.trunc(data / props.pageSize)
            })
    const { data, error } = useSWR(props.url, fetcher)
    return (
        <div className="row justify-content-center">
          <Pagination count={data} onChange={props.handleChange} showFirstButton showLastButton/>
        </div>
    )
}
