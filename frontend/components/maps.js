import { Router, useRouter } from 'next/router'
import { useState, Component } from 'react'
import { Loader } from '@googlemaps/js-api-loader'
import { Map, GoogleApiWrapper, InfoWindow, Marker } from 'google-maps-react'

const mapStyles = {
    width: '50%',
    height: '50%',
}

export class MapContainer extends Component {
    myLatLng = { lat: this.props.lat, lng: this.props.lng }
    render() {
        return (
            <Map
                google={this.props.google}
                zoom={14}
                style={mapStyles}
                initialCenter={{
                    lat: this.props.lat,
                    lng: this.props.lng,
                }}
            >
                <Marker name={this.props.name} position={this.myLatLng} />
            </Map>
        )
    }
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyAVMNYIgj9vgPsHZ9XRKkYY0UJI-I9JPeI',
})(MapContainer)
