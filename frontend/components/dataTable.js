import useSWR from 'swr'
import { useState } from 'react'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'
import { Spinner } from 'react-bootstrap'
import { Router, useRouter } from 'next/router'

function getTable(cols, formatter) {
    let temp = []
    for (let i = 0; i < cols.length; ++i) {
        if (i === 0)
            temp.push(
                <TableHeaderColumn
                    isKey
                    dataField={cols[i].field}
                    dataFormat={formatter}
                    width="250"
                >
                    {cols[i].label}
                </TableHeaderColumn>
            )
        else
            temp.push(
                <TableHeaderColumn dataField={cols[i].field}>
                    {cols[i].label}
                </TableHeaderColumn>
            )
    }
    return temp
}

export default function DataTable(props) {
    const fetcher = (...args) =>
        fetch(...args)
            .then((res) => res.json())
            .then((json) => props.handleData(json))
    const { data, error } = useSWR(props.url, fetcher)
    return (
        <div>
            {data ? (
                <BootstrapTable data={data}>
                    {getTable(props.columns, props.formatter)}
                </BootstrapTable>
            ) : error ? (
                <div>Sorry, something went wrong on our end. </div>
            ) : (
                <Spinner animation="border" />
            )}
        </div>
    )
}
