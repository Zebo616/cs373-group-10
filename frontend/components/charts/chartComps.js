import {
    BarChart,
    Bar,
    Cell,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ScatterChart,
    Scatter,
    AreaChart,
    Area,
    PieChart,
    Pie,
} from 'recharts'

function ParkChart(props) {
    return (
        <AreaChart width={400} height={300} data={props.data}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Area
                type="monotone"
                dataKey="count"
                stroke="#8884d8"
                fill="#8884d8"
            />
        </AreaChart>
    )
}

function AnimalTypeChart(props) {
    return (
        <BarChart width={400} height={300} data={props.data}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis
                yAxisId="left"
                domain={['auto', 'auto']}
                orientation="left"
                stroke="#8884d8"
            />
            <Tooltip />
            <Bar yAxisId="left" dataKey="count" fill="#8884d8" name="Count" />
        </BarChart>
    )
}

function PriceChart(props) {
    return (
        <AreaChart width={400} height={300} data={props.data}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Area
                type="monotone"
                dataKey="count"
                stroke="#8884d8"
                fill="#8884d8"
            />
        </AreaChart>
    )
}

function PopulationChart(props) {
    return (
        <ScatterChart width={400} height={300}>
            <CartesianGrid />
            <XAxis type="number" dataKey="veteran" name="Veteran Population" />
            <YAxis
                type="number"
                dataKey="foreign"
                name="Foreign-Born Population"
            />
            <Tooltip cursor={{ strokeDasharray: '3 3' }} />
            <Scatter name="A city" data={props.data} fill="#8884d8" />
        </ScatterChart>
    )
}

function AidChart(props) {
    return (
        <BarChart width={400} height={300} data={props.data}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" hide />
            <YAxis
                yAxisId="left"
                domain={['auto', 'auto']}
                orientation="left"
                stroke="#8884d8"
            />
            <YAxis
                yAxisId="right"
                domain={['auto', 'auto']}
                orientation="right"
                stroke="#82ca9d"
            />
            <Tooltip />
            <Legend />
            <Bar
                yAxisId="left"
                dataKey="aid"
                fill="#8884d8"
                name="Financial Aid Average"
            />
            <Bar
                yAxisId="right"
                dataKey="sat"
                fill="#82ca9d"
                name="Median SAT Score"
            />
        </BarChart>
    )
}

function PeopleChart(props) {
    return (
        <PieChart width={400} height={400}>
            <Pie
                dataKey="count"
                startAngle={360}
                endAngle={0}
                data={props.data}
                cx={200}
                cy={125}
                outerRadius={100}
                fill="#82ca9d"
                label
            />
            <Tooltip />
        </PieChart>
    )
}

export {
    AidChart,
    ParkChart,
    PopulationChart,
    AnimalTypeChart,
    PriceChart,
    PeopleChart,
}
