import {
    Card,
    Button,
    CardGroup,
    CardDeck,
    Overlay,
    OverlayTrigger,
    Tooltip,
} from 'react-bootstrap'
function OurToolsList(props) {
    return (
        <ul padding-left="100px">
            <OurTool
                link="https://gitlab.com/Zebo616/cs373-group-10"
                src="./gitlab.png"
                title="GitLab"
            />
            <OurTool
                link="https://documenter.getpostman.com/view/12882946/TVYNZvh5"
                src="./postman.png"
                title="Postman"
            />
        </ul>
    )
}
function OurTool(props) {
    return (
        <a href={props.link} target="_blank">
            <img src={props.src} height="150px" width="150px"></img>
        </a>
    )
}

function ToolsList(props) {
    return (
        <ul padding-left="40px">
            <Tool
                link="https://nextjs.org/"
                src="./next-js.png"
                text="Next JS(Frontend Framework)"
            />
            <Tool
                link="https://www.postman.com/"
                src="./postman.png"
                text="Postman (API Documentation)"
            />
            <Tool
                link="https://slack.com/"
                src="./slack.png"
                text="Slack (Team Communication)"
            />
            <Tool
                link="https://gitlab.com/"
                src="./gitlab.png"
                text="GitLab (Code Management)"
            />
            <Tool
                link="https://aws.amazon.com/ec2/"
                src="./aws-ec2.png"
                text="AWS EC2 (App Deployment)"
            />
            <Tool
                link="https://aws.amazon.com/rds/"
                src="./aws-rds.png"
                text="AWS RDS (Database Hosting)"
            />
            <Tool
                link="https://www.heidisql.com/"
                src="./heidisql.png"
                text="HeidiSQL (Database Management)"
            />
            <Tool
                link="https://mochajs.org/"
                src="./mochajs.png"
                text="Mocha (Javascript Test)"
            />
            <Tool
                link="https://www.selenium.dev/"
                src="./selenium.png"
                text="Selenium (Website Test)"
            />
            <Tool
                link="https://www.algolia.com/"
                src="./algolia.png"
                text="Algolia (Searching and Highlighting)"
            />
            <Tool
                link="swr.vercel.app"
                src="./swr.png"
                text="swr (Data Fetching)"
            />
        </ul>
    )
}

function Tool(props) {
    return (
        <>
            {['bottom'].map((placement) => (
                <OverlayTrigger
                    key={placement}
                    placement={placement}
                    overlay={
                        <Tooltip id={`tooltip-${placement}`}>
                            {props.text}
                        </Tooltip>
                    }
                >
                    <a href={props.link} target="_blank">
                        <img src={props.src} height="60px" />
                    </a>
                </OverlayTrigger>
            ))}
        </>
    )
}

export { OurToolsList, ToolsList }
