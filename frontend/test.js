// calls multiple unit test functions
function test(props) {
    return testCities(props) && testColleges(props) && testRestaurants(props)
}

// unit test that checks the information grabbed for Austin, TX.
function testCities(props) {
    // <h1>{props.city[1]}, {props.city[2]}</h1>
    //             <h6>Population: {props.city[8]}</h6>
    //             <h6>Male Population: {props.city[9]}</h6>
    //             <h6>Female Population: {props.city[10]}</h6>
    //             <h6>Number of Veterans: {props.city[4]}</h6>
    //             <h6>Number of foreign-born residents: {props.city[5]}</h6>
    //             <h6>Average Household Size: {props.city[6]}</h6>
    //             <h6>Median Age: {props.city[7]}</h6>

    if (!props.city[1].localeCompare('Austin')) return false
    if (!props.city[2].localeCompare('Texas')) return false
    if (!props.city[8].localeCompare('931840')) return false
    if (!props.city[9].localeCompare('475718')) return false
    if (!props.city[10].localeCompare('456122')) return false
    if (!props.city[4].localeCompare('37414')) return false
    if (!props.city[5].localeCompare('181686')) return false
    if (!props.city[6].localeCompare('2.5')) return false
    if (!props.city[7].localeCompare('32.7')) return false
    return true
}

// unit test that checks the information grabbed for Saltwater Grill.
function testRestaurants(props) {
    // <h1>{props.restaurant[1]}</h1>
    //             <h6>Address: {props.restaurant[2]}</h6>
    //             <h6>City: {props.city ? <a href={"/cities/"+props.city[0]}>{props.restaurant[3]} ({props.restaurant[4]})</a> : props.restaurant[3] + "      (" + props.restaurant[4]+")"}</h6>
    //             <h6>Phone Number: {props.restaurant[5]}</h6>
    //             <h6>Coordinates: {props.restaurant[6]}, {props.restaurant[7]}</h6>
    //             <h6>Price Level (1-4): {props.restaurant[8]}</h6>

    if (!props.restaurant[1].localeCompare('Saltwater Grill')) return false
    if (!props.restaurant[2].localeCompare('2017 Post Office Street'))
        return false
    if (!props.restaurant[3].localeCompare('Galveston')) return false
    if (!props.restaurant[4].localeCompare('TX')) return false
    if (!props.restaurant[5].localeCompare('4097623474')) return false
    if (!props.restaurant[6].localeCompare('29.305291')) return false
    if (!props.restaurant[7].localeCompare('94.790262')) return false
    if (!props.restaurant[8].localeCompare('2')) return false
    return true
}

// unit test that checks the information grabbed for The University of Texas at Austin.
function testColleges(props) {
    // <h1>{props.college[1]}</h1>
    //             <h6>Location: {props.city? <a href={"/cities/"+props.city[0]}>{props.college[2]}, {props.college[3]}</a> : props.college[2] +", "+   //             props.college[3]}</h6>
    //             <h6>Level: {props.college[4]}</h6>
    //             <h6>Control: {props.college[5]}</h6>
    //             <h6>Number of students: {props.college[9]}</h6>
    //             <h6>Percentage of full-time students: {props.college[10]}%</h6>
    //             <h6>Median SAT Score: {props.college[11]}</h6>
    //             <h6>Average Financial Aid: {props.college[12]}</h6>
    //             <h6>Financial Aid Percentile: {props.college[13]}%</h6>
    //             <iframe src={"https://"+props.college[8]} height="400" width="800" name="demo">

    if (!props.college[2].localeCompare('Austin')) return false
    if (!props.college[3].localeCompare('Texas')) return false
    if (!props.college[4].localeCompare('4-year')) return false
    if (!props.college[5].localeCompare('Public')) return false
    if (!props.college[9].localeCompare('39979')) return false
    if (!props.college[10].localeCompare('92.3')) return false
    if (!props.college[11].localeCompare('1262')) return false
    if (!props.college[12].localeCompare('8627')) return false
    if (!props.college[13].localeCompare('87')) return false
    return true
}
