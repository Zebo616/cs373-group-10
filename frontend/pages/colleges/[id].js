import dynamic from 'next/dynamic'
import Head from 'next/head'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'
import PlacePhoto from '../../components/placePhoto'

const CollegeNavBar = dynamic(() => import('../../components/collegeNavBar'))

const CollegeMap = dynamic(() => import('../../components/maps'))

export async function getServerSideProps(context) {
    let college, city
    let restaurants = []
    // retrieving data on the specific college
    college = await fetch(
        'http://ec2-18-188-243-226.us-east-2.compute.amazonaws.com/college/' +
            context.params.id
    )
        .then((response) => {
            return response.json()
        })
        .then((data) => {
            return data[0]
        })
    // retrieving data on city the college is in
    try {
        city = await fetch(
            'http://ec2-18-188-243-226.us-east-2.compute.amazonaws.com/city/' +
                college[3] +
                '/empty/' +
                college[2]
        ).then((response) => {
            return response.json()
        })
    } catch (err) {
        city = null
    }
    // retrieving data on restaurants that are in the same city
    try {
        let res = await fetch(
            'http://ec2-18-188-243-226.us-east-2.compute.amazonaws.com/restaurant/' +
                city[3] +
                '/' +
                city[1]
        ).then((response) => {
            return response.json()
        })
        res.forEach((element) => {
            let rest = {
                id: element[0],
                name: element[1],
                address: element[2],
                city: element[3],
                state: element[4],
            }
            restaurants.push(rest)
        })
    } catch (err) {
        restaurants = null
    }
    return {
        props: {
            college,
            city,
            restaurants,
        },
    }
}

function CellFormatter(cell, row) {
    return (
        <div>
            <a href={'/restaurants/' + row.id}>{cell}</a>
        </div>
    )
}

export default function Colleges(props) {
    return (
        <div>
            <Head>
                <title>CollegeSearch | {props.college[1]}</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <CollegeNavBar></CollegeNavBar>
            <div className="container" style={{ marginTop: '5vh' }}>
                <h1>{props.college[1]}</h1>
                <h6>
                    Location:{' '}
                    {props.city ? (
                        <a href={'/cities/' + props.city[0]}>
                            {props.college[2]}, {props.college[3]}
                        </a>
                    ) : (
                        props.college[2] + ', ' + props.college[3]
                    )}
                </h6>
                <h6>Level: {props.college[4]}</h6>
                <h6>Control: {props.college[5]}</h6>
                <h6>Number of students: {props.college[9]}</h6>
                <h6>Percentage of full-time students: {props.college[10]}%</h6>
                <h6>Median SAT Score: {props.college[11]}</h6>
                <h6>Average Financial Aid: {props.college[12]}</h6>
                <h6>Financial Aid Percentile: {props.college[13]}%</h6>
                <a href={'https://' + props.college[8]}>
                    {'https://' + props.college[8]}
                </a>
                <br />
                <div className="row ml-1 mb-4" style={{ height: '350px' }}>
                    <CollegeMap
                        lat={props.college[7]}
                        lng={props.college[6]}
                        name={props.college[1]}
                    />
                </div>
                <br />
                <br />
                <PlacePhoto
                    placeInput={props.college[1]}
                    maxHeight={400}
                    maxWidth={400}
                />
                <br />
                <br />
                <h5>Restaurants near {props.college[1]}</h5>
                <BootstrapTable data={props.restaurants} search pagination>
                    <TableHeaderColumn isKey dataField="id">
                        ID
                    </TableHeaderColumn>
                    <TableHeaderColumn
                        dataField="name"
                        dataSort={true}
                        dataFormat={CellFormatter}
                    >
                        Restaurant
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="address" dataSort={true}>
                        Address
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="city" dataSort={true}>
                        City
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="state" dataSort={true}>
                        State
                    </TableHeaderColumn>
                </BootstrapTable>
            </div>
        </div>
    )
}
