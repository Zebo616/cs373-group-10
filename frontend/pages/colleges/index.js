import dynamic from 'next/dynamic'
import Head from 'next/head'
import { useState, useEffect } from 'react'
import DataTable from '../../components/dataTable'
import Filters from '../../components/filters'
import Paginate from '../../components/paginate'
import Select from 'react-select'

const CollegeNavBar = dynamic(() => import('../../components/collegeNavBar'))

const backend =
    'http://ec2-18-188-243-226.us-east-2.compute.amazonaws.com/college'

// const backend = 'http://localhost:8000/college'

function CellFormatter(cell, row) {
    return (
        <div>
            <a href={'colleges/' + row.id}>{cell}</a>
        </div>
    )
}

const cols = [
    { field: 'name', label: 'Name', value: 0 },
    { field: 'city', label: 'City', value: 1 },
    { field: 'state', label: 'State', value: 2 },
    { field: 'latitude', label: 'Latitude', value: 3 },
    { field: 'longitude', label: 'Longitude', value: 4 },
    { field: 'numStudents', label: 'Student Population', value: 5 },
    { field: 'medSatScore', label: 'Median SAT Score', value: 6 },
    { field: 'financialAid', label: 'Financial Aid', value: 7 },
]

const filters = [
    { name: 'Latitude', min: 'latMin', max: 'latMax' },
    { name: 'Longitude', min: 'longMin', max: 'longMax' },
    { name: 'Student Population', min: 'studentMin', max: 'studentMax' },
    { name: 'Median SAT Score', min: 'satMin', max: 'satMax' },
    { name: 'Financial Aid', min: 'financialMin', max: 'financialMax' },
]

function handleData(data) {
    //Rows from the data response
    let rows = data.map((college) => {
        return {
            id: college[0],
            name: college[1],
            city: college[2],
            state: college[3],
            latitude: college[6],
            longitude: college[7],
            numStudents: college[9],
            medSatScore: college[11],
            financialAid: college[12],
        }
    })

    return rows
}

export default function Colleges(props) {
    const [queryParams, setQueryParams] = useState({
        latMin: null,
        latMax: null,
        longMin: null,
        longMax: null,
        studentMin: null,
        studentMax: null,
        satMin: null,
        satMax: null,
        finMin: null,
        finMax: null,
        sortOn: 0,
        sortBy: "asc"
    })
    const [query, setQuery] = useState('')
    const [buttonClick, setButtonClick] = useState(false)
    const signalButtonClick = () => {
        setButtonClick(!buttonClick)
    }
    const [page, setPage] = useState(1)

    useEffect(() => {
        let tempQuery = ''
        for (const [key, value] of Object.entries(queryParams)) {
            if (value || (key == 'sortOn' && value > -1)) tempQuery += key+'='+value+'&'
        }
        tempQuery += 'page=' + page
        setQuery('?' + tempQuery)
    }, [buttonClick, page])

    return (
        <div>
            <Head>
                <title>CollegeSearch | Colleges</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <CollegeNavBar />
            <div className="container" style={{ marginTop: '5vh' }}>
                <div className="row justify-content-center">
                    <h1>Colleges</h1>
                </div>
                <Filters
                    filters={filters}
                    queryParams={queryParams}
                    signal={signalButtonClick}
                    columns={cols}
                />
                <br/>
                <div className="row justify-content-center">
                    <DataTable
                        url={backend + query}
                        handleData={handleData}
                        formatter={CellFormatter}
                        columns={cols}
                    />
                </div>
                <br />
                <div>
                    <Paginate
                        url={backend + '/count' + query}
                        pageSize={20}
                        handleChange={(e, v) => {
                            setPage(v)
                        }}
                    />
                    
                </div>
                <br />
            </div>
        </div>
    )
}
