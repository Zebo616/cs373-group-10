import dynamic from 'next/dynamic'
import Head from 'next/head'
import { Container, Col } from 'react-bootstrap'
import {
    AidChart,
    ParkChart,
    PopulationChart,
    AnimalTypeChart,
    PriceChart,
    PeopleChart,
} from '../../components/charts/chartComps'

const CollegeNavBar = dynamic(() => import('../../components/collegeNavBar'))

const backend = 'http://ec2-18-188-243-226.us-east-2.compute.amazonaws.com'
const collegeStatsQuery = '/college?page=1&pageSize=10&sortOn=7&sortBy=desc'
const cityStatsQuery = '/city?page=2&pageSize=100&sortOn=6&sortBy=desc'
const restaurantStatsQuery = '/restaurant?page=1&pageSize=500'
const wildlife = 'http://campcatalog.me/api/wildlife'
const parks = 'http://campcatalog.me/api/parks'
const people = 'http://campcatalog.me/api/historical_people'

export async function getStaticProps() {
    const collegeStats = await fetch(backend + collegeStatsQuery)
        .then((res) => res.json())
        .then((json) => {
            let rows = json.map((college) => {
                return { name: college[1], sat: college[11], aid: college[12] }
            })
            return rows
        })
    const cityStats = await fetch(backend + cityStatsQuery)
        .then((res) => res.json())
        .then((json) => {
            let rows = json.map((city) => {
                return {
                    name: city[1],
                    veteran: Math.round((city[4] * 1000.0) / city[8]) / 1000,
                    foreign: Math.round((city[5] * 1000.0) / city[8]) / 1000,
                }
            })
            return rows
        })
    const restaurantStats = await fetch(backend + restaurantStatsQuery)
        .then((res) => res.json())
        .then((json) => {
            let rows = [
                { name: 1, count: 0 },
                { name: 2, count: 0 },
                { name: 3, count: 0 },
                { name: 4, count: 0 },
            ]
            for (let i = 0; i < json.length; ++i) {
                rows[json[i][8] - 1].count++
            }
            return rows
        })
    const wildlifeStats = await fetch(wildlife)
        .then((res) => res.json())
        .then((json) => {
            return json.objects
        })
        .then((data) => {
            let type = [
                { name: 'Fish', count: 0 },
                { name: 'Mammal', count: 0 },
                { name: 'Bird', count: 0 },
            ]
            let native = [
                { name: 'native', count: 0 },
                { name: 'nonnative', count: 0 },
            ]
            for (let i = 0; i < data.length; ++i) {
                switch (data[i].category) {
                    case 'Bird':
                        type[2].count++
                        break
                    case 'Mammal':
                        type[1].count++
                        break
                    case 'Fish':
                        type[0].count++
                        break
                }
                switch (data[i].nativeness) {
                    case 'Native':
                        native[0].count++
                        break
                    case 'Non-native':
                        native[1].count++
                        break
                }
            }
            return { type, native }
        })
    const parkStats = await fetch(parks)
        .then((res) => res.json())
        .then((json) => {
            return json.objects
        })
        .then((data) => {
            let rows = [
                { name: 'Free', count: 0 },
                { name: '0.01-10.00', count: 0 },
                { name: '10.00-30.00', count: 0 },
                { name: '30.00+', count: 0 },
            ]
            for (let i = 0; i < data.length; ++i) {
                let fee = data[i]['fee']
                    ? parseFloat(data[i]['fee'].substring(1))
                    : 0.0
                if (fee === 0.0) rows[0].count++
                else if (fee > 0.0 && fee <= 10.0) rows[1].count++
                else if (fee > 10.0 && fee <= 30.0) rows[2].count++
                else rows[3].count++
            }
            return rows
        })
    const peopleStats = await fetch(people)
        .then((res) => res.json())
        .then((json) => {
            return json.objects
        })
        .then((data) => {
            let peopleStats = []
            data.forEach((person) => {
                if (!(person.related_parks.length - 1 in peopleStats))
                    peopleStats[person.related_parks.length - 1] = {
                        name:
                            'Num. People With ' +
                            person.related_parks.length +
                            ' Related Park(s)',
                        count: 1,
                    }
                else peopleStats[person.related_parks.length - 1].count += 1
            })
            return peopleStats
        })
    return {
        props: {
            collegeStats,
            cityStats,
            restaurantStats,
            wildlifeStats,
            parkStats,
            peopleStats,
        },
    }
}

export default function Charts(props) {
    return (
        <div>
            <Head>
                <title>CollegeSearch | Charts</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <CollegeNavBar />
            <br />
            <Container fluid>
                <h3>Our Visualizations</h3>
                <div className="row">
                    <Col>
                        <h5>Top Financial Aid Schools vs. Median SAT Score</h5>
                        <AidChart data={props.collegeStats} />
                    </Col>
                    <Col>
                        <h5>Veteran Population vs. Foreign Born</h5>
                        <PopulationChart data={props.cityStats} />
                    </Col>
                    <Col>
                        <h5>Restaurant Price Levels</h5>
                        <PriceChart data={props.restaurantStats} />
                    </Col>
                </div>
                <br />
                <h3>Provider Visualizations</h3>
                <div className="row">
                    <Col>
                        <h5>Types of Animals in Parks</h5>
                        <AnimalTypeChart data={props.wildlifeStats.type} />
                    </Col>
                    <Col>
                        <h5>National Park Fees</h5>
                        <ParkChart data={props.parkStats} />
                    </Col>
                    <Col>
                        <h5>Historical People vs. Number of Related Parks</h5>
                        <PeopleChart data={props.peopleStats} />
                    </Col>
                </div>
            </Container>
        </div>
    )
}
