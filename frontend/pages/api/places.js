// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import Cors from 'cors'
import initMiddleware from '../../lib/init-middleware'
import useSWR from 'swr'

const apiKey = encodeURIComponent('AIzaSyAVMNYIgj9vgPsHZ9XRKkYY0UJI-I9JPeI')

const fetcher = (...args) => fetch(...args).then((res) => res.json())

const cors = initMiddleware(
    // You can read more about the available options here: https://github.com/expressjs/cors#configuration-options
    Cors({
        // Only allow requests with GET, POST and OPTIONS
        methods: ['GET', 'OPTIONS'],
    })
)

export default async function handler(req, res) {
    await cors(req, res)

    const { placeInput, maxHeight, maxWidth } = req.query
    console.log(
        'placeInput: ',
        placeInput,
        ' maxWidth: ',
        maxWidth,
        ' maxHeight: ',
        maxHeight
    )

    if (
        placeInput == undefined ||
        maxHeight == undefined ||
        maxWidth == undefined
    ) {
        res.status(500)
        res.end(
            JSON.stringify({
                message:
                    'Undefined query parameter - either placeInput, maxHeight, or maxWidth',
            })
        )
    } else {
        const inputURI = encodeURIComponent(placeInput)
        try {
            const { candidates } = await fetch(
                `https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=${inputURI}&inputtype=textquery&fields=photos&key=${apiKey}`
            ).then((response) => {
                return response.json()
            })

            const { photos } = candidates[0] //Getting first photo info from the findPlace request
            // console.log(photos)
            res.status(200)
            res.setHeader('Content-Type', 'application/json')
            const url = `https://maps.googleapis.com/maps/api/place/photo?maxwidth=${maxWidth}&maxheight=${maxHeight}&photoreference=${photos[0].photo_reference}&key=${apiKey}`
            res.end(
                JSON.stringify({
                    url: url,
                    html_attributions: photos[0].html_attributions,
                })
            )
        } catch (err) {
            console.log('Places API error ', err)
            res.status(500)
            res.end(
                JSON.stringify({
                    message: 'No photos found for given placeInput',
                })
            )
        }
    }
}
