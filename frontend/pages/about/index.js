import dynamic from 'next/dynamic'
import { OurToolsList, ToolsList } from '../../components/aboutTools'
import { Component } from 'react'
import Head from 'next/head'
import { Container, Row } from 'react-bootstrap'

const CollegeNavBar = dynamic(() => import('../../components/collegeNavBar'))

/*
const renderTooltip = (props) => (
    <Tooltip id="button-tooltip" {...props}>
      
    </Tooltip>
  );
  */

export async function getStaticProps() {
    const commits = await fetch(
        'https://gitlab.com/api/v4/projects/21390373/repository/commits?per_page=500'
    ).then((response) => {
        return response.json()
    })
    const issues = await fetch(
        'https://gitlab.com/api/v4/projects/21390373/issues?per_page=500'
    ).then((response) => {
        return response.json()
    })
    let users = {
        steven: { numCommits: 0, numIssues: 0 },
        christopher: { numCommits: 0, numIssues: 0 },
        keegan: { numCommits: 0, numIssues: 0 },
        anshuman: { numCommits: 0, numIssues: 0 },
        katherine: { numCommits: 0, numIssues: 0 },
    }
    for (let i = 0; i < commits.length; ++i) {
        switch (commits[i].author_name) {
            case 'Steven Callahan':
                users.steven.numCommits++
                break
            case 'Christopher Chen':
                users.christopher.numCommits++
                break
            case 'KeeganFranklin':
                users.keegan.numCommits++
                break
            case 'Anshuman Kumar':
                users.anshuman.numCommits++
                break
            case 'Katherine H':
                users.katherine.numCommits++
                break
            default:
                users.keegan.numCommits++
                break
        }
    }
    for (let i = 0; i < issues.length; ++i) {
        switch (issues[i].author.name) {
            case 'Steven Callahan':
                users.steven.numIssues++
                break
            case 'Christopher Chen':
                users.christopher.numIssues++
                break
            case 'KeeganFranklin':
                users.keegan.numIssues++
                break
            case 'Anshuman Kumar':
                users.anshuman.numIssues++
                break
            case 'Katherine Hsu':
                users.katherine.numIssues++
                break
            default:
                users.keegan.numIssues++
                break
        }
    }
    return {
        props: {
            gitlab: {
                users,
                totalCommits: commits.length,
                totalIssues: issues.length,
            },
        },
    }
}

function About({ gitlab }) {
    return (
        <div>
            <Head>
                <title>CollegeSearch | About</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <CollegeNavBar></CollegeNavBar>
            <div className="container text-center" style={{ marginTop: '5vh' }}>
                <h1>About CollegeSearch</h1>
                <h6>
                    This website is an online database to assist you in find the
                    college that is best suit for you,
                </h6>
                <h6>all according to your preferences!</h6>
                <br />
                <h3>The Team</h3>
                <br />
                <Container fluid>
                    <Row>
                        <div className="col text-center">
                            <h5>
                                Steven Callahan{' '}
                                <a
                                    href="https://www.linkedin.com/in/steven-callahan-000335191/"
                                    target="_blank"
                                >
                                    <img src="./linkedin.png" />
                                </a>
                            </h5>
                            <img src="./steven.jpeg" height="300" />
                            <h6>
                                I am a second year Computer Science major at UT
                                with an anticipated minor in business. I have a
                                specialization in big data and growing passions
                                for cybersecurity and game design.
                            </h6>
                            <h6>Major Responsibilities: Data, Javascript</h6>
                            <h6>Commits: {gitlab.users.steven.numCommits}</h6>
                            <h6>Issues: {gitlab.users.steven.numIssues}</h6>
                            <h6>Unit Tests: 3</h6>
                        </div>
                        <div className="col">
                            <h5>
                                Christopher Chen{' '}
                                <a
                                    href="https://www.linkedin.com/in/chrischen2191/"
                                    target="_blank"
                                >
                                    <img src="./linkedin.png" />
                                </a>
                            </h5>
                            <img src="./chris.JPG" height="300" />
                            <h6>
                                I'm a third year computer science major at the
                                University of Austin. I love my dog Paco and
                                music more than anything else.
                            </h6>
                            <h6>
                                Major Responsibilities: Frontend, Backend,
                                Database, Docker
                            </h6>
                            <h6>
                                Commits: {gitlab.users.christopher.numCommits}
                            </h6>
                            <h6>
                                Issues: {gitlab.users.christopher.numIssues}
                            </h6>
                            <h6>Unit Tests: 0</h6>
                        </div>
                        <div className="col">
                            <h5>
                                Keegan Franklin{' '}
                                <a
                                    href="https://www.linkedin.com/in/keegan-franklin-426ba5170/"
                                    target="_blank"
                                >
                                    <img src="./linkedin.png" />
                                </a>
                            </h5>
                            <img src="./keegan.jpeg" height="300" />
                            <h6>
                                I'm a Junior majoring in Computer Science at the
                                University of Texas. I am originally from
                                Amarillo, Texas. In my free time, I enjoy
                                practicing the violin, volunteering and finding
                                new board games to play with friends.
                            </h6>
                            <h6>
                                Major Responsibilities: API, Selenium, Python
                            </h6>
                            <h6>Commits: {gitlab.users.keegan.numCommits}</h6>
                            <h6>Issues: {gitlab.users.keegan.numIssues}</h6>
                            <h6>Unit Tests: 9</h6>
                        </div>
                        <div className="col">
                            <h5>
                                Anshuman Kumar{' '}
                                <a
                                    href="https://www.linkedin.com/in/anshuman-k-b5a037121/"
                                    target="_blank"
                                >
                                    <img src="./linkedin.png" />
                                </a>
                            </h5>
                            <h6>Team Leader</h6>
                            <img src="./anshuman.jpg" height="300" />
                            <h6>
                                Hey, I'm Anshuman Kumar, and I'm a senior at the
                                University of Texas at Austin. I'll be
                                graduating this December and will be starting
                                work full time at Amazon in the spring.
                            </h6>
                            <h6>
                                Major Responsibilities: Deployment, Backend, API
                                Test
                            </h6>
                            <h6>Commits: {gitlab.users.anshuman.numCommits}</h6>
                            <h6>Issues: {gitlab.users.anshuman.numIssues}</h6>
                            <h6>Unit Tests: 0</h6>
                        </div>
                        <div className="col">
                            <h5>
                                Katherine Hsu{' '}
                                <a
                                    href="https://www.linkedin.com/in/katherine-hsu/"
                                    target="_blank"
                                >
                                    <img src="./linkedin.png" />
                                </a>
                            </h5>

                            <img src="./katherine.jpg" height="300" />
                            <h6>
                                Hi, I'm Katherine Hsu and I'm from Austin,
                                Texas. I'm a junior Computer Science major at UT
                                Austin this year. Outside of schoolwork, I'm in
                                a few clubs and I also like to bullet journal,
                                listen to music, watch youtube, and paint.
                            </h6>
                            <h6>Major Responsibilities: API, Python</h6>
                            <h6>
                                Commits: {gitlab.users.katherine.numCommits}
                            </h6>
                            <h6>Issues: {gitlab.users.katherine.numIssues}</h6>
                            <h6>Unit Tests: 9</h6>
                        </div>
                    </Row>
                </Container>
                <br />
                <h4>Total Commits: {gitlab.totalCommits}</h4>
                <h4>Total Issues: {gitlab.totalIssues}</h4>
                <br />
                <h4>Tools Used</h4>
                <div>
                    <ToolsList></ToolsList>
                </div>
                <br />
                <h4>Our Links</h4>
                <div>
                    <OurToolsList></OurToolsList>
                </div>
                <div>
                    Linkedin icons made by{' '}
                    <a
                        href="https://www.flaticon.com/authors/freepik"
                        title="Freepik"
                    >
                        Freepik
                    </a>{' '}
                    from{' '}
                    <a href="https://www.flaticon.com/" title="Flaticon">
                        www.flaticon.com
                    </a>
                </div>
            </div>
        </div>
    )
}

export default About
