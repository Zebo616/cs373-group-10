import dynamic from 'next/dynamic'
import Head from 'next/head'
import algoliasearch from 'algoliasearch/lite'
import {
    InstantSearch,
    Hits,
    SearchBox,
    Pagination,
    Highlight,
    Index,
    Configure,
} from 'react-instantsearch-dom'
import { Router, useRouter } from 'next/router'

const CollegeNavBar = dynamic(() => import('../../components/collegeNavBar'))

const searchClient = algoliasearch(
    '1S67PDW4CD',
    '9e3a3e34c5ac8b9619de6fb7466a7703'
)

function College(props) {
    return (
        <div>
            <div className="hit-name">
                <h3>
                    <a href={'/colleges/' + props.hit.id}>
                        <Highlight
                            attribute="name"
                            tagName="mark"
                            hit={props.hit}
                        />
                    </a>
                </h3>
            </div>
            <div>
                <span>Location: </span>
                <Highlight attribute="city" tagName="mark" hit={props.hit} />
                <span>, </span>
                <Highlight attribute="state" tagName="mark" hit={props.hit} />
            </div>
        </div>
    )
}

function Restaurant(props) {
    return (
        <div>
            <div className="hit-name">
                <h3>
                    <a href={'/restaurants/' + props.hit.id}>
                        <Highlight
                            attribute="name"
                            tagName="mark"
                            hit={props.hit}
                        />
                    </a>
                </h3>
            </div>
            <div>
                <span>Location: </span>
                <Highlight attribute="city" tagName="mark" hit={props.hit} />
                <span>, </span>
                <Highlight attribute="state" tagName="mark" hit={props.hit} />
            </div>
        </div>
    )
}

function City(props) {
    return (
        <div>
            <div className="hit-name">
                <h3>
                    <a href={'/cities/' + props.hit.id}>
                        <Highlight
                            attribute="city"
                            tagName="mark"
                            hit={props.hit}
                        />
                        <span>, </span>
                        <Highlight
                            attribute="state"
                            tagName="mark"
                            hit={props.hit}
                        />
                    </a>
                </h3>
            </div>
            <div>
                <span>Population: {props.hit.population}</span>
            </div>
        </div>
    )
}

export default function Search(props) {
    const router = useRouter()
    return (
        <div>
            <Head>
                <title>CollegeSearch | Search Results</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <CollegeNavBar />
            <div className="mx-2 mt-2">
                <InstantSearch
                    indexName="college"
                    searchClient={searchClient}
                    searchState={{ query: router.query.query }}
                >
                    <div style={{ display: 'none' }}>
                        <SearchBox />
                    </div>
                    <Index indexName="college">
                        <h1>Colleges</h1>
                        <Hits hitComponent={College} />
                        <Configure hitsPerPage={16} />
                        <br />
                    </Index>
                    <Index indexName="city">
                        <h1>Cities</h1>
                        <Hits hitComponent={City} />
                        <Configure hitsPerPage={16} />
                        <br />
                    </Index>
                    <Index indexName="restaurant">
                        <h1>Restaurants</h1>
                        <Hits hitComponent={Restaurant} />
                        <Configure hitsPerPage={16} />
                        <br />
                    </Index>
                </InstantSearch>
            </div>
        </div>
    )
}
