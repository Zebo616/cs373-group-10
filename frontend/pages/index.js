import Head from 'next/head'
import dynamic from 'next/dynamic'
import {
    InputGroup,
    DropdownButton,
    Dropdown,
    FormControl,
} from 'react-bootstrap'

const CollegeNavBar = dynamic(() => import('../components/collegeNavBar'))

export default function Home() {
    const splashStyling = {
        marginTop: '15%',
    }
    return (
        <div
            className=""
            style={{
                height: '100vh',
                backgroundImage: `url('./splashpage.jpg')`,
            }}
        >
            <Head>
                <title>CollegeSearch</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <CollegeNavBar></CollegeNavBar>
            <div className="container" style={splashStyling}>
                <div className="row justify-content-center">
                    <h1>collegesearch.me</h1>
                </div>
                <div className="row justify-content-center">
                    <h5>find the best college for you.</h5>
                </div>
            </div>
        </div>
    )
}
