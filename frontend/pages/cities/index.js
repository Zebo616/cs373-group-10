import dynamic from 'next/dynamic'
import Head from 'next/head'
import { useEffect, useState } from 'react'
import DataTable from '../../components/dataTable'
import Filters from '../../components/filters'
import Paginate from '../../components/paginate'

const CollegeNavBar = dynamic(() => import('../../components/collegeNavBar'))

const backend = 'http://ec2-18-188-243-226.us-east-2.compute.amazonaws.com/city'
// const backend = 'http://localhost:8000/city'

function CellFormatter(cell, row) {
    return (
        <div>
            <a href={'cities/' + row.id}>{cell}</a>
        </div>
    )
}

const cols = [
    { field: 'city', label: 'City', value: 0 },
    { field: 'state', label: 'State', value: 1 },
    { field: 'state_code', label: 'State Code', value: 2 },
    { field: 'population', label: 'Population', value: 3 },
    { field: 'num_veterans', label: 'Veteran Population', value: 4 },
    { field: 'foreign_born', label: 'Foreign Born Population', value: 5 },
    { field: 'average_household', label: 'Average Household Size', value: 6 },
    { field: 'median_age', label: 'Median Age', value: 7 },
]

const filters = [
    { name: 'Population', min: 'popMin', max: 'popMax' },
    { name: 'Veteran Population', min: 'vetMin', max: 'vetMax' },
    { name: 'Foreign Born Population', min: 'foreignMin', max: 'foreignMax' },
    { name: 'Average Household Size', min: 'houseMin', max: 'houseMax' },
    { name: 'Median Age', min: 'ageMin', max: 'ageMax' },
]

function handleData(data) {
    //Rows from the data response
    let rows = data.map((city) => {
        return {
            id: city[0],
            city: city[1],
            state: city[2],
            state_code: city[3],
            population: city[8],
            num_veterans: city[4],
            foreign_born: city[5],
            average_household: city[6],
            median_age: city[7],
        }
    })
    return rows
}

export default function Cities(props) {
    const [queryParams, setQueryParams] = useState({
        popMin: null,
        popMax: null,
        vetMin: null,
        vetMax: null,
        foreignMin: null,
        foreignMax: null,
        houseMin: null,
        houseMax: null,
        ageMin: null,
        ageMax: null,
        sortOn: 0,
        sortBy: "asc"
    })
    const [query, setQuery] = useState('')
    const [buttonClick, setButtonClick] = useState(false)
    const signalButtonClick = () => {
        setButtonClick(!buttonClick)
    }
    const [page, setPage] = useState(1)

    useEffect(() => {
        let tempQuery = ''
        for (const [key, value] of Object.entries(queryParams)) {
            if (value || (key == 'sortOn' && value > -1)) tempQuery += key + '=' + value + '&'
        }
        tempQuery += 'page=' + page
        setQuery('?' + tempQuery)
    }, [buttonClick, page])

    return (
        <div>
            <Head>
                <title>CollegeSearch | Cities</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <CollegeNavBar></CollegeNavBar>
            <div className="container" style={{ marginTop: '5vh' }}>
                <div className="row justify-content-center">
                    <h1>Cities</h1>
                </div>
                <Filters
                    filters={filters}
                    queryParams={queryParams}
                    signal={signalButtonClick}
                    columns={cols}
                />
                <br></br>
                <div className="row justify-content-center">
                    <DataTable
                        url={backend + query}
                        handleData={handleData}
                        formatter={CellFormatter}
                        columns={cols}
                    />
                </div>
                <br />
                <div>
                    <Paginate
                        url={backend + '/count' + query}
                        pageSize={20}
                        handleChange={(e, v) => {
                            setPage(v)
                        }}
                    />
                </div>
                <br />
            </div>
        </div>
    )
}
