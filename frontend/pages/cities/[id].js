import dynamic from 'next/dynamic'
import Head from 'next/head'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'
import PlacePhoto from '../../components/placePhoto'

const CollegeNavBar = dynamic(() => import('../../components/collegeNavBar'))
var averageCollegeAid = 'default'
var averageRestaurantPrice = 'default'

export async function getServerSideProps(context) {
    let colleges = []
    let restaurants = []
    const city = await fetch(
        'http://ec2-18-188-243-226.us-east-2.compute.amazonaws.com/city/' +
            context.params.id
    )
        .then((response) => {
            return response.json()
        })
        .then((data) => {
            return data[0]
        })
    try {
        var totalColleges = 0
        var totalAid = 0
        let res = await fetch(
            'http://ec2-18-188-243-226.us-east-2.compute.amazonaws.com/college/' +
                city[2] +
                '/' +
                city[1]
        ).then((response) => {
            return response.json()
        })
        res.forEach((element) => {
            let college = {
                id: element[0],
                name: element[1],
                city: element[2],
                state: element[3],
                site: element[8],
            }
            totalColleges += 1
            totalAid += element[12]
            colleges.push(college)
        })
        if (totalColleges > 0) {
            averageCollegeAid = (totalAid / totalColleges).toFixed(2)
        } else {
            averageCollegeAid = 'N/A'
        }
    } catch (err) {
        averageCollegeAid = 'error'
        colleges = null
    }
    try {
        var totalRestaurants = 0
        var totalPrice = 0
        let res = await fetch(
            'http://ec2-18-188-243-226.us-east-2.compute.amazonaws.com/restaurant/' +
                city[3] +
                '/' +
                city[1]
        ).then((response) => {
            return response.json()
        })
        res.forEach((element) => {
            let rest = {
                id: element[0],
                name: element[1],
                address: element[2],
                city: element[3],
                state: element[4],
            }
            totalRestaurants += 1
            totalPrice += element[8]
            restaurants.push(rest)
        })
        if (totalRestaurants > 0) {
            averageRestaurantPrice = (totalPrice / totalRestaurants).toFixed(2)
        } else {
            averageRestaurantPrice = 'N/A'
        }
    } catch (err) {
        averageRestaurantPrice = 'error'
        restaurants = null
    }
    const cityData = {
        ACA: averageCollegeAid,
        ARP: averageRestaurantPrice,
    }
    return {
        props: {
            city,
            colleges,
            restaurants,
            cityData,
        },
    }
}

function CollegeFormatter(cell, row) {
    return (
        <div>
            <a href={'/colleges/' + row.id}>{cell}</a>
        </div>
    )
}

function RestaurantFormatter(cell, row) {
    return (
        <div>
            <a href={'/restaurants/' + row.id}>{cell}</a>
        </div>
    )
}

export default function Cities(props) {
    return (
        <div>
            <Head>
                <title>CollegeSearch | {props.city[1]}</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <CollegeNavBar></CollegeNavBar>
            <div className="container" style={{ marginTop: '5vh' }}>
                <h1>
                    {props.city[1]}, {props.city[2]}
                </h1>
                <h6>Population: {props.city[8]}</h6>
                <h6>Male Population: {props.city[9]}</h6>
                <h6>Female Population: {props.city[10]}</h6>
                <h6>Number of Veterans: {props.city[4]}</h6>
                <h6>Number of foreign-born residents: {props.city[5]}</h6>
                <h6>Average Household Size: {props.city[6]}</h6>
                <h6>Median Age: {props.city[7]}</h6>
                <h6>Average College Financial Aid: $ {props.cityData.ACA}</h6>
                <h6>Average Restaurant Price Index: {props.cityData.ARP}</h6>
                <br />
                <br />

                <PlacePhoto
                    placeInput={props.city[1] + ', ' + props.city[2]}
                    maxHeight={400}
                    maxWidth={400}
                />
                <h5>
                    Colleges in {props.city[1]}, {props.city[2]}
                </h5>
                <BootstrapTable data={props.colleges} search pagination>
                    <TableHeaderColumn isKey dataField="id">
                        ID
                    </TableHeaderColumn>
                    <TableHeaderColumn
                        dataField="name"
                        dataSort={true}
                        dataFormat={CollegeFormatter}
                    >
                        Name
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="city" dataSort={true}>
                        City
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="state" dataSort={true}>
                        State
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="site" dataSort={true}>
                        Website
                    </TableHeaderColumn>
                </BootstrapTable>
                <br />
                <br />
                <h5>
                    Restaurants in {props.city[1]}, {props.city[2]}
                </h5>
                <BootstrapTable data={props.restaurants} search pagination>
                    <TableHeaderColumn isKey dataField="id">
                        ID
                    </TableHeaderColumn>
                    <TableHeaderColumn
                        dataField="name"
                        dataSort={true}
                        dataFormat={RestaurantFormatter}
                    >
                        Restaurant
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="address" dataSort={true}>
                        Address
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="city" dataSort={true}>
                        City
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="state" dataSort={true}>
                        State
                    </TableHeaderColumn>
                </BootstrapTable>
            </div>
        </div>
    )
}
