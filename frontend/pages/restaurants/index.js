import dynamic from 'next/dynamic'
import Head from 'next/head'
import { useEffect, useState } from 'react'
import Filters from '../../components/filters'
import DataTable from '../../components/dataTable'
import Paginate from '../../components/paginate'

const CollegeNavBar = dynamic(() => import('../../components/collegeNavBar'))

const backend = 'http://ec2-18-188-243-226.us-east-2.compute.amazonaws.com/restaurant'
// const backend = 'http://localhost:8000/restaurant'

function CellFormatter(cell, row) {
    return (
        <div>
            <a href={'restaurants/' + row.id}>{cell}</a>
        </div>
    )
}

const cols = [
    {field: 'name', label: 'Restaurant', value: 0},
    {field: 'address', label: 'Address', value: 1},
    {field: 'city', label: 'City', value: 2},
    {field: 'state', label: 'State', value: 3},
    {field: 'latitude', label: 'Latitude', value: 4},
    {field: 'longitude', label: 'Longitude', value: 5},
    {field: 'price', label: 'Price Level (1-4)', value: 6}
]

const filters = [
    { name: 'Latitude', min: 'latMin', max: 'latMax' },
    { name: 'Longitude', min: 'longMin', max: 'longMax' },
    { name: 'Price Level', min: 'priceMin', max: 'priceMax' },
]

function handleData(data) {
    let rows = data.map((restaurant) => {
        return {
            id: restaurant[0],
            name: restaurant[1],
            address: restaurant[2],
            city: restaurant[3],
            state: restaurant[4],
            latitude: restaurant[6],
            longitude: restaurant[7],
            price: restaurant[8],
        }
    })
    return rows
}

export default function Restaurants(props) {
    const [queryParams, setQueryParams] = useState({
        latMin: null,
        latMax: null,
        longMin: null,
        longMax: null,
        priceMin: null,
        priceMax: null,
        sortOn: null,
        sortBy: null,
        sortOn: 0,
        sortBy: "asc"
    })
    const [query, setQuery] = useState('')
    const [buttonClick, setButtonClick] = useState(false)
    const signalButtonClick = () => {
        setButtonClick(!buttonClick)
    }
    const [page, setPage] = useState(1)

    useEffect(() => {
        let tempQuery = ''
        for (const [key, value] of Object.entries(queryParams)) {
            if (value || (key == 'sortOn' && value > -1)) tempQuery += key + '=' + value + '&'
        }
        tempQuery += 'page=' + page
        setQuery('?' + tempQuery)
    }, [buttonClick, page])

    return (
        <div>
            <Head>
                <title>CollegeSearch | Restaurants</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <CollegeNavBar></CollegeNavBar>
            <div className="container" style={{ marginTop: '5vh' }}>
                <div className="row justify-content-center">
                    <h1>Restaurants</h1>
                </div>
                <Filters
                    filters={filters}
                    queryParams={queryParams}
                    signal={signalButtonClick}
                    columns={cols}
                />
                <br></br>
                <div className="row justify-content-center">
                    <DataTable
                        url={backend + query}
                        handleData={handleData}
                        formatter={CellFormatter}
                        columns={cols}
                    />
                </div>
                <br />
                <div>
                    <Paginate
                        url={backend + '/count' + query}
                        pageSize={20}
                        handleChange={(e, v) => {
                            setPage(v)
                        }}
                    />
                </div>
                <br />
            </div>
        </div>
    )
}
