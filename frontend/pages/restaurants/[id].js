import dynamic from 'next/dynamic'
import Head from 'next/head'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'
import PlacePhoto from '../../components/placePhoto'

const CollegeNavBar = dynamic(() => import('../../components/collegeNavBar'))

const RestaurantMap = dynamic(() => import('../../components/maps'))

export async function getServerSideProps(context) {
    let city
    let colleges = []
    const restaurant = await fetch(
        'http://ec2-18-188-243-226.us-east-2.compute.amazonaws.com/restaurant/' +
            context.params.id
    )
        .then((response) => {
            return response.json()
        })
        .then((data) => {
            return data[0]
        })
    try {
        city = await fetch(
            'http://ec2-18-188-243-226.us-east-2.compute.amazonaws.com/city/empty/' +
                restaurant[4] +
                '/' +
                restaurant[3]
        ).then((response) => {
            return response.json()
        })
    } catch (err) {
        city = null
    }
    try {
        let res = await fetch(
            'http://ec2-18-188-243-226.us-east-2.compute.amazonaws.com/college/' +
                city[2] +
                '/' +
                city[1]
        ).then((response) => {
            return response.json()
        })
        res.forEach((element) => {
            let college = {
                id: element[0],
                name: element[1],
                city: element[2],
                state: element[3],
                site: element[8],
            }
            colleges.push(college)
        })
    } catch (err) {
        colleges = null
    }
    return {
        props: {
            restaurant,
            city,
            colleges,
        },
    }
}

function CollegeFormatter(cell, row) {
    return (
        <div>
            <a href={'/colleges/' + row.id}>{cell}</a>
        </div>
    )
}

export default function Restaurant(props) {
    return (
        <div>
            <Head>
                <title>CollegeSearch | {props.restaurant[1]}</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <CollegeNavBar></CollegeNavBar>
            <div className="container" style={{ marginTop: '5vh' }}>
                <h1>{props.restaurant[1]}</h1>
                <h6>Address: {props.restaurant[2]}</h6>
                <h6>
                    City:{' '}
                    {props.city ? (
                        <a href={'/cities/' + props.city[0]}>
                            {props.restaurant[3]} ({props.restaurant[4]})
                        </a>
                    ) : (
                        props.restaurant[3] + ' (' + props.restaurant[4] + ')'
                    )}
                </h6>
                <h6>Phone Number: {props.restaurant[5]}</h6>
                <h6>
                    Coordinates: {props.restaurant[6]}, {props.restaurant[7]}
                </h6>
                <h6>Price Level (1-4): {props.restaurant[8]}</h6>
                <iframe
                    src={
                        props.restaurant[9].substring(0, 4) +
                        's' +
                        props.restaurant[9].substring(4)
                    }
                    height="400"
                    width="800"
                    name="demo"
                >
                    Your website doesn't support iframe!
                </iframe>
                <div className="row ml-1 mb-4" style={{ height: '350px' }}>
                    <RestaurantMap
                        lat={props.restaurant[6]}
                        lng={props.restaurant[7]}
                        name={props.restaurant[1]}
                    />
                </div>
                <br />
                <br />
                <PlacePhoto
                    placeInput={props.restaurant[1]}
                    maxHeight={400}
                    maxWidth={400}
                />
                <br />
                <br />
                <h5>Colleges near {props.restaurant[1]}</h5>
                <BootstrapTable data={props.colleges} search pagination>
                    <TableHeaderColumn isKey dataField="id">
                        ID
                    </TableHeaderColumn>
                    <TableHeaderColumn
                        dataField="name"
                        dataSort={true}
                        dataFormat={CollegeFormatter}
                    >
                        Name
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="city" dataSort={true}>
                        City
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="state" dataSort={true}>
                        State
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="site" dataSort={true}>
                        Website
                    </TableHeaderColumn>
                </BootstrapTable>
            </div>
        </div>
    )
}
