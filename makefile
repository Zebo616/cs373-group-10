.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

BLACK         := black
CHECKTESTDATA := checktestdata
COVERAGE      := coverage3
MYPY          := mypy
PYDOC         := pydoc3
PYLINT        := pylint
PYTHON        := python3
NODE          := node
MOCHA         := mocha
PYTEST        := pytest
NPM           := npm

all:

testJS :
	# Only run the install line if one of the following is not installed; Otherwise the pipeline will slow down significantly
	npm install mocha prettier
	cd frontend/ && mocha && npx prettier --check

testPY :
	# Only run the install line if one of the following is not installed; Otherwise the pipeline will slow down significantly
	pip install SQLAlchemy && pip install flask_sqlalchemy && pip install pymysql
	pip install pytest && pip install flask && pip install jsonify && pip install Flask && pip install flask-cors
	cd backend/ && pytest -s

testAPI:
	cd API-docs-testing/
	newman run collegeAPI.postman_collection.json -e dev.postman_environment.json --bail

start :
	#$(NPM) run build ./frontend
	cd frontend/ && $(NPM) run build
	$(PYTHON) ./backend/main_test.py
