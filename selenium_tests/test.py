import unittest
import time
from selenium import webdriver

# PATH = '/Users/keeganfranklin/Documents/cs373Downing/cs373-group-10/selenium_tests/chromedriver'
PATH = "./selenium_tests/chromedriver"

class SeleniumTests(unittest.TestCase): 

    def setUp(self): 
        self.driver = webdriver.Chrome(PATH)

    def test_main_page(self):
        self.driver.get("https://collegesearch.me/")
        a = self.driver.find_element_by_class_name("navbar-brand")
        assert a.text == "collegesearch.me"
        self.driver.close()

    def test_about_page(self): 
       self.driver = webdriver.Chrome(PATH)
       self.driver.get("https://collegesearch.me/")
       self.driver.find_element_by_class_name("navbar-brand").click()
       a = self.driver.find_element_by_tag_name('h1')
       assert a.text == "About CollegeSearch"
       self.driver.close()

    def test_college_data(self): 
        self.driver = webdriver.Chrome(PATH) 
        self.driver.get("https://collegesearch.me/")
        self.driver.find_element_by_class_name("dropdown-toggle.nav-link").click()
        self.driver.find_element_by_class_name("dropdown-item").click()
        a = self.driver.find_element_by_tag_name('h1')
        assert a.text == "Colleges"
        self.driver.close

if __name__ == "__main__": 
    unittest.main()